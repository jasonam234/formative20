package com.jason.login.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.jason.login.entity.Users;

@Transactional
public interface UsersRepository extends JpaRepository<Users, Integer>{
	@Query(value = "SELECT * FROM Users", nativeQuery = true)
	List<Users> getAllUsers();
	
	@Query(value = "SELECT * FROM Users WHERE id = ?1", nativeQuery = true)
	Users getUserById(long id);
	
	@Query(value = "SELECT * FROM Users WHERE username = ?1", nativeQuery = true)
	List<Users> getUserByUsername(String username);
	
	@Query(value = "SELECT * FROM Users WHERE password = ?1", nativeQuery = true)
	List<Users> getUserByPassword(String password);
	
	@Query(value = "SELECT * FROM Users WHERE username = ?1", nativeQuery = true)
	Users getUserByUsernameObject(String username);

	@Modifying
	@Query(value = "INSERT INTO Users(username, password) VALUES(?1 ,?2)", nativeQuery = true)
	int postUser(String username, String password);
}
