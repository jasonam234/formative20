package com.jason.login.helper;

import java.util.List;

import com.jason.login.entity.Users;

public class UserResponse implements Response{
	int status;
	String message;
	List<Users> data;
	
	public UserResponse(int status, String message, List<Users> data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}
	
	public UserResponse(int status, String message) {
		super();
		this.status = status;
		this.message = message;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<Users> getData() {
		return data;
	}

	public void setData(List<Users> data) {
		this.data = data;
	}
	
	@Override
	public String toString() {
		return "UserResponse [status=" + status + ", message=" + message + ", data=" + data + "]";
	}
}
