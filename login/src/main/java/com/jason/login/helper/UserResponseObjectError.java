package com.jason.login.helper;

import com.jason.login.entity.Users;

public class UserResponseObjectError implements Response{
	int status;
	String message;
	
	public UserResponseObjectError(int status, String message) {
		super();
		this.status = status;
		this.message = message;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
