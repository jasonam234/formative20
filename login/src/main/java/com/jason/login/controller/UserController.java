package com.jason.login.controller;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.jason.login.entity.Users;
import com.jason.login.helper.Response;
import com.jason.login.helper.UserResponse;
import com.jason.login.helper.UserResponseAuthentication;
import com.jason.login.helper.UserResponseObject;
import com.jason.login.helper.UserResponseObjectError;
import com.jason.login.repository.UsersRepository;

@RestController
public class UserController {
	@Autowired
	UsersRepository userRepository;
	
	@GetMapping(value = "/api/users", produces = "application/json")
	public ResponseEntity<UserResponse> getAllUsers(){
		List<Users> data = userRepository.getAllUsers();
		UserResponse response = new UserResponse(HttpStatus.OK.value(), "GET all users operation succesful", data);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@GetMapping(value = "/api/user/{id}", produces = "application/json")
	public ResponseEntity<Response> getUserById(@PathVariable int id){
		Users data = userRepository.getUserById(Long.valueOf(id));
		Response response = new UserResponseObject(HttpStatus.OK.value(), "GET user by ID operation succesful", data);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@PostMapping(value = "/api/user", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> postNewUser(@RequestBody Users payload) throws NoSuchAlgorithmException{
		List<Users> validationOne = userRepository.getUserByUsername(payload.getUsername());
		if(validationOne.size() > 0) {
			Response response = new UserResponseObjectError(HttpStatus.CONFLICT.value(), "Username already exists !");
			return ResponseEntity.status(HttpStatus.CONFLICT).body(response);
		}
		
	    MessageDigest md = MessageDigest.getInstance("MD5");
	    md.update(payload.getPassword().getBytes());
	    byte[] digest = md.digest();
	    String myHash = DatatypeConverter.printHexBinary(digest).toUpperCase();
		
		int data = userRepository.postUser(payload.getUsername(), myHash);
		Users lastUser = userRepository.getUserByUsernameObject(payload.getUsername());

		Response response = new UserResponseObject(HttpStatus.ACCEPTED.value(), "POST user operation succesful", lastUser);
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(response);
	}
	
	@PostMapping(value = "/api/auth/login", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> userValidation(@RequestBody List<Users> payload) throws NoSuchAlgorithmException{
		List<Users> validationOne = userRepository.getUserByUsername(payload.get(0).getUsername());
		
		MessageDigest md = MessageDigest.getInstance("MD5");
	    md.update(payload.get(0).getPassword().getBytes());
	    byte[] digest = md.digest();
	    String myHash = DatatypeConverter.printHexBinary(digest).toUpperCase();
		List<Users> validationTwo = userRepository.getUserByPassword(myHash);
		
		if(validationOne.size() == 1 && validationTwo.size() == 1) {
			Response response = new UserResponseAuthentication(HttpStatus.OK.value(), "POST user login operation succesful");
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(response);
		}else {
			Response response = new UserResponseAuthentication(HttpStatus.OK.value(), "Wrong Username or Password");
			return ResponseEntity.status(HttpStatus.FORBIDDEN).body(response);
		}
	}
}
