package com.jason.login.helper;

import com.jason.login.entity.Users;

public class UserResponseObject implements Response{
	int status;
	String message;
	Object data;
	
	public UserResponseObject(int status, String message, Users data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}
	
	public UserResponseObject(int status, String message) {
		super();
		this.status = status;
		this.message = message;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Users data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "UserResponseObjectUsers [status=" + status + ", message=" + message + ", data=" + data + "]";
	}
}
