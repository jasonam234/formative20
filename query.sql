show databases;
Drop database users;
create database users;
use users;

CREATE TABLE Users(
	id int UNIQUE AUTO_INCREMENT,
    username varchar(30),
    password varchar(1000),
    
    PRIMARY KEY (id)
);

INSERT INTO Users(username, password) VALUES
("user_one", "password_one"),
("user_two", "password_two");

SELECT * FROM Users;