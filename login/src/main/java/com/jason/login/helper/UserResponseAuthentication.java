package com.jason.login.helper;

public class UserResponseAuthentication implements Response{
	int status;
	String message;
	
	public UserResponseAuthentication(int status, String message) {
		super();
		this.status = status;
		this.message = message;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "UserResponseAuthentication [status=" + status + ", message=" + message + "]";
	}
}
